use clap::{Parser, Subcommand};
use colored::Colorize;
use std::{fs, io::Write, path::PathBuf, process, thread, time::Duration, write};
use toml::Value;

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    New {
        #[arg(short, long)]
        name: String,
    },
    Build,
    Run,
    Publish,
}

fn log(text: &str) {
    println!("{} {}", "INFO".yellow().bold(), text);
}
fn success(text: &str) {
    println!("{} {}", "SUCCESS".green().bold(), text);
}
fn error(text: &str) {
    println!("{} {}", "ERROR".red().bold(), text);
}

fn create_project(name: String) {
    let mut path = PathBuf::from(&name);
    fs::create_dir(&path).unwrap();
    log(&format!("Created new folder `{}`", name.blue()));
    path.push("config.toml");
    let mut config = fs::File::create(&path).unwrap();
    write!(
        config,
        "[package]
name = \"{}\"
source = \"src\"
build = \"build\"\n
[publish]
output = \"release\"
version = \"0.1\"
data = []\n",
        name
    )
    .unwrap();
    log(&format!("Written default {}", "config.toml".blue()));
    path.pop();
    path.push("build");
    fs::create_dir(&path).unwrap();
    log(&format!("Created {} directory", "build".blue()));
    path.pop();
    path.push("release");
    fs::create_dir(&path).unwrap();
    log(&format!("Created {} directory", "release".blue()));
    path.pop();
    path.push("src");
    fs::create_dir(&path).unwrap();
    log(&format!("Created {} directory", "src".blue()));
    path.push("main.sps");
    let mut file = fs::File::create(&path).unwrap();
    write!(
        file,
        "[
    \"NARRATOR\" \"Hello, World!\" !
] \"main\" def
\"main\" call\n"
    )
    .unwrap();
    log(&format!("Written default {}", "src/main.sps".blue()));
    success(&format!("Project `{}` created", name.blue()));
}

fn build() {
    if let Ok(text) = fs::read_to_string("config.toml") {
        log("Found config.toml, building project");
        let config: Value = toml::from_str(&text).unwrap();
        if let Value::Table(config) = config {
            let package = config.get("package").unwrap();
            if let Value::Table(package) = package {
                let build_dir = package.get("build").unwrap().as_str().unwrap();
                let source_dir = package.get("source").unwrap().as_str().unwrap();
                let package_name = package.get("name").unwrap().as_str().unwrap();
                process::Command::new("senpy")
                    .arg("compile")
                    .arg("-s")
                    .arg(format!("{}/main.sps", source_dir))
                    .arg("-o")
                    .arg(format!("{}/{}", build_dir, package_name))
                    .spawn()
                    .expect("Failed to run senpy");
            }
        }
    } else {
        error("config.toml not found");
    }
}

fn run() {
    build();
    thread::sleep(Duration::from_millis(300));
    if let Ok(text) = fs::read_to_string("config.toml") {
        log("Found config.toml, running project");
        let config: Value = toml::from_str(&text).unwrap();
        if let Value::Table(config) = config {
            let package = config.get("package").unwrap();
            if let Value::Table(package) = package {
                let build_dir = package.get("build").unwrap().as_str().unwrap();
                let package_name = package.get("name").unwrap().as_str().unwrap();
                process::Command::new("senpy")
                    .arg("execute")
                    .arg("-s")
                    .arg(format!("{}/{}", build_dir, package_name))
                    .status()
                    .expect("Failed to run senpy");
            }
        }
    } else {
        error("config.toml not found");
    }
}

const PUBLISH_PATH: &str = "/tmp/senpack_publish";

fn publish() {
    build();
    thread::sleep(Duration::from_millis(300));
    if let Ok(text) = fs::read_to_string("config.toml") {
        fs::remove_dir_all(PUBLISH_PATH).unwrap_or(());
        fs::create_dir(PUBLISH_PATH).unwrap();
        log("Created publish directory");
        let config: Value = toml::from_str(&text).unwrap();
        let config = config.as_table().unwrap();
        let package = config.get("package").unwrap().as_table().unwrap();
        let publish = config.get("publish").unwrap().as_table().unwrap();

        let build_dir = package.get("build").unwrap().as_str().unwrap();
        let package_name = package.get("name").unwrap().as_str().unwrap();
        let binary = format!("{}/{}", build_dir, package_name);
        let publish_dir = publish.get("output").unwrap().as_str().unwrap();
        process::Command::new("cp")
            .arg(binary)
            .arg(format!("{}/{}", PUBLISH_PATH, package_name))
            .spawn()
            .unwrap()
            .wait()
            .unwrap();
        log("Copied binary to publish folder");
        for file in publish.get("data").unwrap().as_array().unwrap() {
            process::Command::new("touch")
                .arg(format!("{}/{}", PUBLISH_PATH, file.as_str().unwrap()))
                .spawn()
                .unwrap()
                .wait()
                .unwrap();
        }
        log("Created data files");
        let output_name = format!(
            "{}-{}.tar.gz",
            package_name,
            publish.get("version").unwrap().as_str().unwrap()
        );
        let mut proc = process::Command::new("tar");
        proc.arg("czf")
            .arg(format!("{}/{}", publish_dir, output_name))
            .arg("-C")
            .arg(PUBLISH_PATH);
        for file in publish.get("data").unwrap().as_array().unwrap() {
            proc.arg(file.as_str().unwrap());
        }
        proc.arg(package_name);
        proc.spawn().unwrap().wait().unwrap();
        success("Publish complete");
    }
}

fn main() {
    let args = Args::parse();
    match args.command {
        Command::New { name } => create_project(name),
        Command::Build => build(),
        Command::Run => run(),
        Command::Publish => publish(),
    }
}
